package com.udemy.backendninja.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.servlet.view.RedirectView;

import com.udemy.backendninja.service.EjercicioService;

@Controller
@RequestMapping("/ejercicio")
public class EjercicioController {

	@Autowired
	@Qualifier("ejercicioService")
	private EjercicioService ejercicioService;
	
	@GetMapping("/metodo1")
	public String metodo1() {
		return "redirect:/ejercicio/metodo2";
	}
//	public RedirectView metodo1() {
//		return new RedirectView("/ejercicio/metodo2");
//	}
	
	@GetMapping("/metodo2")
	public String metodo2() {
		ejercicioService.loggear();
		return "example2";
	}
	
}
