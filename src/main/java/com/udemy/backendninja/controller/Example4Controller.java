package com.udemy.backendninja.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/example4")
public class Example4Controller {
	
	private static final String VIEW = "500";
	
	@GetMapping("/someview")
	public String someView() {
		return VIEW;
	}
	
}
