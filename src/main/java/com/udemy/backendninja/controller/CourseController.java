package com.udemy.backendninja.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.udemy.backendninja.converter.CourseConverter;
import com.udemy.backendninja.entity.Course;
import com.udemy.backendninja.model.CourseModel;
import com.udemy.backendninja.service.CourseService;

@Controller
@RequestMapping("/courses")
public class CourseController {

	private static final String COURSES_VIEW = "courses";
	private static final Log LOG = LogFactory.getLog(CourseController.class);
	private static final CourseConverter cc = new CourseConverter();

	@Autowired
	@Qualifier("courseService")
	private CourseService courseService;

	@GetMapping("/listcourses")
	public ModelAndView listAllCourses() {
		LOG.info("Call: listAllCourses()");
		ModelAndView mav = new ModelAndView(COURSES_VIEW);
		List<Course> courses = courseService.listAllCourses();
		mav.addObject("courses", courses.stream().map(course -> cc.entityToModel(course)).collect(Collectors.toList()));
		mav.addObject("course", new CourseModel());
		return mav;
	}

	@PostMapping("/addcourse")
	public String addCourse(@ModelAttribute("course") CourseModel courseModel) {
		LOG.info("Call: addCourse() -- Param: " + courseModel.toString());
		courseService.addCourse(cc.modelToEntity(courseModel));
		return "redirect:/courses/listcourses";
	}

}
